﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DualSliderTestController : MonoBehaviour {
	
	public void OnChangedRangeValue( DualSlider.RangeValue rangeValue )
	{
		m_LeftText.text = rangeValue.Min.ToString();
		m_RightText.text = rangeValue.Max.ToString();
	}

	public Text m_LeftText;
	public Text m_RightText;
}
